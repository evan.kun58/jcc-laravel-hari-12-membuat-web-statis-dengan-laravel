<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST"> 
        @csrf
        <label>First name:</label><br>
        <input type="text" name="firstname"><br><br>

        <label>Last name:</label><br>
        <input type="text" name="lastname"><br><br>
        
        <label>Gender:</label><br>
        <input type="radio" name="gender" value="man">man <br>
        <input type="radio" name="gender" value="woman">woman <br>
        <input type="radio" name="gender" value="other">other <br><br>

        <label>Nationality:</label><br>
        <select name="nationality" id="">
            <option value="1">Indonesian</option>
            <option value="2">Singaporean</option>
            <option value="3">Malaysian</option>
            <option value="4">Australian</option>
        </select><br><br>

        
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="language" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="2">English <br>
        <input type="checkbox" name="language" value="3">Arabic <br>
        <input type="checkbox" name="language" value="4">Japanese <br><br>

        <label>Bio:</label><br>
        <textarea name="biography" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="sign up">
    </form>
</body>
</html>